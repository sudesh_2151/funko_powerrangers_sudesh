import React from 'react';
import {Grid, Col, Row, Image} from 'react-bootstrap';
require('styles/footer.css');

const videourl = "https://www.youtube.com/embed/XGSy3_Czz8k"
class Footer extends React.Component {
  render() {
    return (  
      <footer> 
          <Grid fluid={true}>
          <Row>
            <Col md={2} xs={12}>
              <div >
               <Image className="logoImg" src= "../../images/funko.png"/>
              </div>
              <hr/>
              <div>
                <div>
                1202 Shuksan Way
                </div>
                <div>
                  Everett, WA 98203
                </div>
                <div>
                  contact@funko.com
                </div>
                <div>
                  425-783-3616
                </div>
              </div>
            </Col>
            <Col md={2} xs={12}>
              <div >
                  <h4>Browser</h4>
              </div>
              <hr/>
              <div>
                <div>
                  Products
                </div>
                <div>
                  Licenses
                </div>
                <div>
                  Find a Store
                </div>
                <div>
                  Sign Up
                </div>
                <div>
                  Shop
                </div>
              </div>  
            </Col>
            <Col md={2} xs={12} >
              <div >
                  <h4>FUNKO</h4>
              </div>
              <hr/>
              <div>
                <div>
                  About Us
                </div>
                <div>
                  Careers
                </div>
                <div>
                  Blog
                </div>
                <div>
                  Funko Shop
                </div>
                <div>
                  Contact
                </div>
                <div>
                  Forum
                </div>
                <div>
                  Sweepstack Rules
                </div>
                <div>
                  Funko News Room
                </div>
              </div>       
            </Col>
            <Col md={3} xs={12}>
            <div >
                  <h4>News Letter</h4>
              </div>
              <hr/>
              <div>
              Sign up for the Funko Newsletter for access to exclusive content!
              </div>
            </Col>
          </Row>
        </Grid>
      </footer>
    );
  }
}

Footer.defaultProps = {
};
  
export default Footer;
