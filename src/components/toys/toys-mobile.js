import React from 'react';
import {Grid, Col, Row, Image, Button, Thumbnail}  from 'react-bootstrap';
require('styles/toys.css');

class ToysMobile extends React.Component {
  render() {
    return (  
      <div className="toys-mobile">   
        
		<Grid>
		    <Row>
		    <Col>
		    	<div className="toy-message"> COMING TO A PHONE NEAR YOU.<br/> JUMP ON QUIDD AND GET THEM BEFORE THEY'RE GONE</div>
		    </Col>
		    <Col>
		    	<div className="m_download">
		    		<Button className="download_button"> Download Now </Button>
		    	</div>
		    </Col>
		    <Col>
				    <Grid bsClass="toysImages">
				    <Row>
				    <Col xs={4}>
				      <Thumbnail src="../../images/red-power-ranger.png" alt="242x200">
				        
				      </Thumbnail>
				    </Col>
				    <Col xs={4}>
				      <Thumbnail src="../../images/pink-power-ranger-m.png" alt="242x200">
				      </Thumbnail>
				    </Col>
				    <Col xs={4}>
				      <Thumbnail src="../../images/yellow-power-ranger-m.png" alt="242x200">
				      </Thumbnail>
				    </Col>
				    </Row>
				    <Row>
				    <Col xs={4}>
				      <Thumbnail src="../../images/black-power-ranger-m.png" alt="242x200">
				        
				      </Thumbnail>
				    </Col>
				    <Col xs={4}>
				      <Thumbnail src="../../images/blue-power-ranger-m.png" alt="242x200">
				      </Thumbnail>
				    </Col>
				    <Col xs={4}>
				      <Thumbnail src="../../images/rita-power-ranger-m.png" alt="242x200">
				      </Thumbnail>
				    </Col>
				    </Row>
				</Grid> 

		    </Col>
		    
		    </Row>
		</Grid> 
      </div>
    );
  }
}

ToysMobile.defaultProps = {
};

export default ToysMobile;
