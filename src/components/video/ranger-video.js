import React from 'react';
require('styles/video.css');

const videourl = "https://www.youtube.com/embed/XGSy3_Czz8k"
class RangerVideo extends React.Component {
  render() {
    return (  
      <div className="ranger-video">   
        <div className="toy-message"> IT'S MORPHIN TIME! GO, GO POWER UP IN THEATERS MARCH 24<sup>TH</sup></div>
        <div className="youtube-video-container">  
           <iframe className="youtube-iframe"
            src={videourl}>
            </iframe>
        </div>  
      </div>
    );
  }
}

RangerVideo.defaultProps = {
};

export default RangerVideo;
